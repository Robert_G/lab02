//Robert Gutkowski 1810515
public class BikeStore 
{
    public static void main(String[] args)
    {
        Bicycle[] rakeOfBicycles = createBicycles();
        
        for(int i = 0; i < rakeOfBicycles.length; i++)
        {
            System.out.println(rakeOfBicycles[i]+"\n");
        }
    }

    public static Bicycle[] createBicycles()
    {
        Bicycle[] rakeOfBicycles = new Bicycle[4];
        rakeOfBicycles[0] = new Bicycle("Motorola", 5, 100);
        rakeOfBicycles[1] = new Bicycle("Flamin' Tricycles", 15, 1000000);
        rakeOfBicycles[2] = new Bicycle("Time warping bikes", 100, 123456);
        rakeOfBicycles[3] = new Bicycle("Little babies' motor bike", 2, 10);
        return rakeOfBicycles;
    }
}
